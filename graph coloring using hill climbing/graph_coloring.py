
#1511799
#ThanhLongDo

class USA_coloring:
    def __init__(self,matrix):
        state=self.initial_state(matrix)
        self.legal_move(state,matrix)
        print(state)
        
    #initial state is each node which is drawed by -1
    #so we have a list [-1,-1,-1,....]
    def initial_state(self,matrix):
        state=[]
        for i in range(len(matrix[0])):
            state+=[-1]
        return state
        
    def legal_move(self,state,matrix):
        while True:
            index=self.value_func(matrix,state)
            #print(index)
            neibourgh_color_set=[]
            
            for j in range(len(state)):
                if matrix[index][j]==1:
                    if state[j]!=-1:
                        neibourgh_color_set.append(state[j])

            for color in range(0,len(state)):
                if self.exist_in_list(neibourgh_color_set,color)==False:
                    state[index]=color
                    break
            print(state)
            if self.exist_in_list(state,-1)==False:
                break
                
            
    def exist_in_list(self,lst,element):
        if not lst:
            return False
        for i in range(len(lst)):
            if element==lst[i]:
                return True
        return False
    
    #in my experience to play this game: coloring the node which have largest degree
    #this function will valuate next state, and move to the best state in set of nextstate
    def value_func(self,matrix,state):
        row=0;value=0
        for i in range(len(matrix)):
            degree=0
            if state[i]==-1:
                for j in range(len(matrix)):
                    degree+=matrix[i][j]
                if degree>value and state[i]==-1:
                    value=degree;
                    row=i;
                #print(degree)
        return row

matrix=[[0,1,0,0,0,0,1,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [1,0,1,1,0,0,1,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,1,0,1,1,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,1,1,0,1,1,1,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,0,1,0,0,0,1, 1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],

        [0,0,0,1,1,0,1,0,1,1, 1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [1,1,0,1,0,1,0,1,1,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,1,0,1,0, 0,0,0,0,0,1,1,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,1,1,1,0,1, 0,0,0,0,1,1,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,0,0,1,0, 1,0,1,1,1,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],

        [0,0,0,0,1,1,0,0,0,1, 0,1,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 1,0,1,0,0,0,0,0,0,0, 1,1,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,1, 1,1,0,1,0,0,0,0,0,1, 1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,1, 0,0,1,0,1,0,0,0,0,1, 1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,1,1, 0,0,0,1,0,1,0,0,1,1, 0,0,0,0,0,0,0,0,0,0],

        [0,0,0,0,0,0,0,1,1,0, 0,0,0,0,1,0,1,1,1,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,1,1,0, 0,0,0,0,0,1,0,1,0,0, 0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,1,1,0,1,0, 0,0,0,0,0,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,1,0,1,0,1, 0,0,0,0,0,1,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,1,1,1,0,0,0,1,0, 1,0,0,1,1,1,0,0,0,0],

        [0,0,0,0,0,0,0,0,0,0, 0,1,1,0,0,0,0,0,0,1, 0,1,1,1,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,1,0,0,0,0,0,0,0,0, 1,0,1,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 1,1,0,1,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,1, 1,0,1,0,1,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,1, 0,0,0,1,0,1,1,0,0,1],

        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,1,1, 0,0,0,0,1,0,1,1,0,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,1,0,0,1,1],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,1,0, 0,0,0,0,0,1,0,0,1,0],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,1,1,0,1],
        [0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,1,0,1,0]]


USA_coloring(matrix)
