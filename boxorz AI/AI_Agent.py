
import sys
import numpy as np
from State import *

#AI to find solution
class AI_agent:
    def __init__(self, initial_state, goal_state):
        self.initial_state=initial_state
        self.goal_state=goal_state

    def DFS_move(self, in_state,id=-1,stop=False):
        state=in_state[:]
        self.print(state)
        if stop==True:
            return
        elif self.check_legal(state) == False:
            return
        elif self.Is_goal(state)==True:
            stop=True
            return

        if id!=1:
            self.up_move(state)
            self.formalize(state)
            self.DFS_move(state,1)
        elif id!=2:
            self.down_move(state)
            self.formalize(state)
            self.DFS_move(state,2)
        elif id!=3:
            self.left_move(state)
            self.formalize(state)
            self.DFS_move(state,3)
        elif id!=4:
            self.right_move(state)
            self.formalize(state)
            self.DFS_move(state,4)

    def BFS_move(self):
        pass
    def Is_goal(self, state):
        if state==self.goal_state:
            return True
        return False
    
    def formalize(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]!=U:
                    state[i][j]=self.initial_state[i][j]
    
    def up_move(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U and self.Block_stand(state)==True: 
                    state[i-2][j]=U
                    state[i-1][j]=U

                    state[i][j]=X
                elif state[i][j]==U and self.Block_stand(state)==False: 
                    state[i-1][j]=U

                    state[i][j]=X
                    state[i+1][j]=X

    def down_move(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U and self.Block_stand(state)==True: 
                    state[i+2][j]=U
                    state[i+1][j]=U

                    state[i][j]=X
                else:
                    state[i+1][j]=U

                    state[i][j]=X
                    state[i-1][j]=X
    def left_move(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U and self.Block_stand(state)==True: 
                    state[i][j-2]=U
                    state[i][j-1]=U

                    state[i][j]=X
                else:
                    state[i][j-1]=U

                    state[i][j]=X
                    state[i][j+1]=X
    def right_move(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U and self.Block_stand(state)==True: 
                    state[i][j+2]=U
                    state[i][j+1]=U

                    state[i][j]=X
                else:
                    state[i][j+1]=U

                    state[i][j]=X
                    state[i][j-1]=X

    def Block_stand(self, state):
        count=0
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U:
                    count=count+1
        if count==1:
            return True
        return False

    def check_legal(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                if state[i][j]==U and self.initial_state[i][j]==X:
                    return False
        return True

    def print(self,state):
        for i in range(len(state)):
            for j in range(len(state[0])):
                sys.stdout.write(str(state[i][j]))
            sys.stdout.write("\n")
        print("-----------------------------------------")
