
#define initial state and goal state for game

X=0#background

O=1#board

B=120#easy to break

C=2# button

T=4#target hole

U=3#block stand up

initial_state_level1 = [[X, X, X, X, X, X, X, X, X, X, X, X],
                        [X, O, O, O, X, X, X, X, X, X, X, X],
                        [X, O, U, O, O, O, O, X, X, X, X, X],
                        [X, O, O, O, O, O, O, O, O, O, X, X],
                        [X, X, O, O, O, O, O, O, O, O, O, X],
                        [X, X, X, X, X, X, O, O, T, O, O, X],
                        [X, X, X, X, X, X, X, O, O, O, X, X],
                        [X, X, X, X, X, X, X, X, X, X, X, X]]

goal_state_list_1 = [[X, X, X, X, X, X, X, X, X, X, X, X],
                     [X, O, O, O, X, X, X, X, X, X, X, X],
                     [X, O, O, O, O, O, O, X, X, X, X, X],
                     [X, O, O, O, O, O, O, O, O, O, X, X],
                     [X, X, O, O, O, O, O, O, O, O, O, X],
                     [X, X, X, X, X, X, O, O, U, O, O, X],
                     [X, X, X, X, X, X, X, O, O, O, X, X],
                     [X, X, X, X, X, X, X, X, X, X, X, X]]