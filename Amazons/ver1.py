
import threading
import time
from collections import deque
# ======================== Class Player =======================================
class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name
       
    def __str__(self):
        return self.str

    def nextMove(self, state):
        start = time.time()
        move_list=self.generate_move(state)
       

        if not move_list:#lose
            return []

        resultMove=self.valuation_func(state,move_list,True,start)
        #print(resultMove)
       
        return [(resultMove[0][0],resultMove[0][1]),(resultMove[1][0],resultMove[1][1]),(resultMove[2][0],resultMove[2][1])]
    
    def valuation_func(self,state, move_list,AI_turn,start_time):
        #self.resultMove=[[0,3],[5,3],[8,6],0]
        
        resultMove=[]
        while move_list:
            
            move=move_list.popleft()

            new_board=self.board_copy(state)
            new_board[move[0]][move[1]]='.'
            new_board[move[2]][move[3]]=self.str

            value_func=self.t1(new_board,AI_turn)#+self.t2(new_board,AI_turn)
            
            if not resultMove:
                resultMove=[[move[0],move[1]],[move[2],move[3]],[move[4],move[5]],value_func]
                #print("result move",resultMove)        

            #exit()
            #file=open("value_func.txt","a")
            #file.write(str(value_func))
            #file.write("\n")
            #file.close()
            #print(value_func)
            if value_func>resultMove[3]:
                resultMove[0][0]=move[0]
                resultMove[0][1]=move[1]
                resultMove[1][0]=move[2]
                resultMove[1][1]=move[3]
                resultMove[2][0]=move[4]
                resultMove[2][1]=move[5]
                resultMove[3]=value_func

            #calculate time, return immediately if time out
            elapse = time.time() - start_time
            #print(elapse)
            if elapse >2.9:
                return resultMove

        #print("move list", move_list)
        return resultMove

    #generate all possible move in one player    
    def generate_move(self,state):

        move_list=deque()#movement resevation list 
        queen_list=deque()#list for saving queen position

        move=[[0,0],[0,0]]
        for i in range(10):
            for j in range(10):
                if state[i][j]==self.str:
                    queen_list.append((i,j))

        #print(queen_list)
        while queen_list:
            (i,j)=queen_list.popleft()
            move[0][0]=i;move[0][1]=j
            
            for index in range(1,10):
                if i-index<0:
                    break
                if state[i-index][j]=='X' or state[i-index][j]=='b' or state[i-index][j]=='w':
                    break
                if i-index>=0 and state[i-index][j]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j]=self.str
                    move[1][0]=i-index;move[1][1]=j
                    move_list+=self.shoot(move,new_board)
                    #print("i,j:",i,j)
                    #print("i-index,j",i-index,j)
                    #print(self.shoot(move,new_board))
            for index in range(1,10):
                if i+index>9:
                    break
                if state[i+index][j]=='X' or state[i+index][j]=='b' or state[i+index][j]=='w':
                    break
                if i+index<=9 and state[i+index][j]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j]=self.str
                    move[1][0]=i+index;move[1][1]=j
                    move_list+=self.shoot(move,new_board)
                    
            for index in range(1,10):    
                if j-index<0:
                    break
                if state[i][j-index]=='X' or state[i][j-index]=='b' or state[i][j-index]=='w':
                    break
                if j-index>=0 and state[i][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i][j-index]=self.str
                    move[1][0]=i;move[1][1]=j-index
                    move_list+=self.shoot(move,new_board)

            for index in range(1,10):
                if j+index>9:
                    break
                if state[i][j+index]=='X' or state[i][j+index]=='b' or state[i][j+index]=='w':
                    break
                if j+index<=9 and state[i][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i][j+index]=self.str
                    move[1][0]=i;move[1][1]=j+index
                    move_list+=self.shoot(move,new_board)
                
            for index in range(1,10):
                if i-index<0 or j-index<0:
                    break
                if state[i-index][j-index]=='X' or state[i-index][j-index]=='b' or state[i-index][j-index]=='w':
                    break
                if i-index>=0 and j-index>=0 and state[i-index][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j-index]=self.str
                    move[1][0]=i-index;move[1][1]=j-index
                    move_list+=self.shoot(move,new_board)
              
            for index in range(1,10):
                if i-index<0 or j+index>9:
                    break
                if state[i-index][j+index]=='X' or state[i-index][j+index]=='b' or state[i-index][j+index]=='w':
                    break
                if i-index>=0 and j+index<=9 and state[i-index][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j+index]=self.str
                    move[1][0]=i-index;move[1][1]=j+index
                    move_list+=self.shoot(move,new_board)
                
            for index in range(1,10):
                if i+index>9 or j-index<0:
                    break
                if state[i+index][j-index]=='X' or state[i+index][j-index]=='b' or state[i+index][j-index]=='w':
                    break
                if i+index<=9 and j-index>=0 and state[i+index][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j-index]=self.str
                    move[1][0]=i+index;move[1][1]=j-index
                    move_list+=self.shoot(move,new_board)
                
            for index in range(1,10):
                if i+index>9 or j+index>9:
                    break
                if state[i+index][j+index]=='X' or state[i+index][j+index]=='b' or state[i+index][j+index]=='w':
                    break
                if i+index<=9 and j+index<=9 and state[i+index][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j+index]=self.str
                    move[1][0]=i+index;move[1][1]=j+index
                    move_list+=self.shoot(move,new_board)
            
            #print(move_list)
            #file=open("result.txt","w")
            #file.write(str(move_list))
            #file.close()
            
        #print("move list",move_list)
        return move_list

    def shoot(self, move, state):

        return_queue=deque()
        i=move[1][0];j=move[1][1]

        for index in range(1,10):
            if i-index<0:
                break
            if state[i-index][j]=='X' or state[i-index][j]=='b' or state[i-index][j]=='w':
                break
            if i-index>=0 and state[i-index][j]=='.':
                #print(move[0][0],move[0][1],move[1][0],move[1][1],i-index,j)
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j))
                    
        for index in range(1,10):
            if i+index>9:
                break
            if state[i+index][j]=='X' or state[i+index][j]=='b' or state[i+index][j]=='w':
                break
            if i+index<=9 and state[i+index][j]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j))
                    
        for index in range(1,10):    
            if j-index<0:
                break
            if state[i][j-index]=='X' or state[i][j-index]=='b' or state[i][j-index]=='w':
                break
            if j-index>=0 and state[i][j-index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i,j-index))

        for index in range(1,10):
            if j+index>9:
                break
            if state[i][j+index]=='X' or state[i][j+index]=='b' or state[i][j+index]=='w':
                break
            if j+index<=9 and state[i][j+index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i,j+index))
                
        for index in range(1,10):
            if i-index<0 or j-index<0:
                break
            if state[i-index][j-index]=='X' or state[i-index][j-index]=='b' or state[i-index][j-index]=='w':
                break
            if i-index>=0 and j-index>=0 and state[i-index][j-index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j-index))
              
        for index in range(1,10):
            if i-index<0 or j+index>9:
                break
            if state[i-index][j+index]=='X' or state[i-index][j+index]=='b' or state[i-index][j+index]=='w':
                break
            if i-index>=0 and j+index<=9 and state[i-index][j+index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j+index))

        for index in range(1,10):
            if i+index>9 or j-index<0:
                break
            if state[i+index][j-index]=='X' or state[i+index][j-index]=='b' or state[i+index][j-index]=='w':
                break
            if i+index<=9 and j-index>=0 and state[i+index][j-index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j-index))
            
        for index in range(1,10):
            if i+index>9 or j+index>9:
                break
            if state[i+index][j+index]=='X' or state[i+index][j+index]=='b' or state[i+index][j+index]=='w':
                break
            if i+index<=9 and j+index<=9 and state[i+index][j+index]=='.':
                return_queue.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j+index))

        return return_queue
    
    #calculate t1. Read article for more information
    def t1(self,state,AI_turn):#for queen terrority
        value_t1=0
        
        if self.str =='w':
            AI_state=self.queen_move(state,"w")
            opposite_state=self.queen_move(state,'b')
        else:
            AI_state=self.queen_move(state,"b")
            opposite_state=self.queen_move(state,'w')
        
        #self.board_print(state)
        #print("ai state:")
        #self.board_print(AI_state)
        #print("opposite state")
        #self.board_print(opposite_state)
        #print("value:",value_t1)
        for i in range(10):
            for j in range(10):
                if state[i][j]=='.':
                    if AI_state[i][j] == opposite_state[i][j] and AI_state[i][j]==100:
                        value_t1+=0
                    elif AI_state[i][j] == opposite_state[i][j] and AI_turn:
                        value_t1+=0.2
                    elif AI_state[i][j] == opposite_state[i][j] and not AI_turn:
                        value_t1-=0.2
                    elif AI_state[i][j] < opposite_state[i][j]:
                        value_t1+=1
                    elif AI_state[i][j] > opposite_state[i][j]:
                        value_t1-=1
                    else:
                        value_t1+=0#do nothing
                    #print("AI_state[",i,"][",j,"]:",AI_state[i][j],"opposite_state[i][j]",opposite_state[i][j])
                    #print("value:",value_t1)
                        
        return value_t1   

    #calculate t2. Read article for more information
    def t2(self,state,AI_turn):#for king terrority
        value_t2=0
        
        if self.str =='w':
            AI_state=self.king_move(state,"w")
            opposite_state=self.king_move(state,'b')
        else:
            AI_state=self.king_move(state,"b")
            opposite_state=self.king_move(state,'w')
        #print("ai state:")
        #self.board_print(AI_state)
        #print("opposite state")
        #self.board_print(opposite_state)
        for i in range(10):
            for j in range(10):
                if state[i][j]=='.':
                    if AI_state[i][j] == opposite_state[i][j] and AI_state[i][j]==100:
                        value_t2+=0
                    elif AI_state[i][j] == opposite_state[i][j] and AI_turn:
                        value_t2+=0.2
                    elif AI_state[i][j] == opposite_state[i][j] and not AI_turn:
                        value_t2-=0.2
                    elif AI_state[i][j] < opposite_state[i][j]:
                        value_t2+=1
                    elif AI_state[i][j] > opposite_state[i][j]:
                        value_t2-=1
                    else:
                        value_t2+=0#do nothing
                        
        #print("value t2:")
        #print(value_t2)
        return value_t2

    def queen_move(self,state,site):
        new_board=self.board_copy(state)
        queue=deque()#very important structure
        number_interation=0

        for i in range(10):
            for j in range(10):
                if new_board[i][j]==site:
                    queue.append((i,j,0))

        while True:
            number_interation+=1
            
            while queue:
                (i,j,numberMove)=queue.popleft()

                if numberMove != 0:
                    new_board[i][j]=numberMove
                
                for index in range(1,10):
                    if i-index<0:
                        break
                    if new_board[i-index][j]=='X' or new_board[i-index][j]=='b' or new_board[i-index][j]=='w':
                        break
                    if i-index>=0 and new_board[i-index][j]=='.':
                        new_board[i-index][j]='t'
                    
                for index in range(1,10):
                    if i+index>9:
                        break
                    if new_board[i+index][j]=='X' or new_board[i+index][j]=='b' or new_board[i+index][j]=='w':
                        break
                    if i+index<=9 and new_board[i+index][j]=='.':
                        new_board[i+index][j]='t'

                for index in range(1,10):    
                    if j-index<0:
                        break
                    if new_board[i][j-index]=='X' or new_board[i][j-index]=='b' or new_board[i][j-index]=='w':
                        break
                    if j-index>=0 and new_board[i][j-index]=='.':
                        new_board[i][j-index]='t'
                
                for index in range(1,10):
                    if j+index>9:
                        break
                    if new_board[i][j+index]=='X' or new_board[i][j+index]=='b' or new_board[i][j+index]=='w':
                        break
                    if j+index<=9 and new_board[i][j+index]=='.':
                        new_board[i][j+index]='t'
                
                for index in range(1,10):
                    if i-index<0 or j-index<0:
                        break
                    if new_board[i-index][j-index]=='X' or new_board[i-index][j-index]=='b' or new_board[i-index][j-index]=='w':
                        break
                    if i-index>=0 and j-index>=0 and new_board[i-index][j-index]=='.':
                        new_board[i-index][j-index]='t'
              
                for index in range(1,10):
                    if i-index<0 or j+index>9:
                        break
                    if new_board[i-index][j+index]=='X' or new_board[i-index][j+index]=='b' or new_board[i-index][j+index]=='w':
                        break
                    if i-index>=0 and j+index<=9 and new_board[i-index][j+index]=='.':
                        new_board[i-index][j+index]='t'
                
                for index in range(1,10):
                    #print("i+index",i+index,"j-index",j-index)
                    if i+index>9 or j-index<0:
                        break
                    if new_board[i+index][j-index]=='X' or new_board[i+index][j-index]=='b' or new_board[i+index][j-index]=='w':
                        break
                    if i+index<=9 and j-index>=0 and new_board[i+index][j-index]=='.':
                        new_board[i+index][j-index]='t'
                
                for index in range(1,10):
                    if i+index>9 or j+index>9:
                        break
                    if new_board[i+index][j+index]=='X' or new_board[i+index][j+index]=='b' or new_board[i+index][j+index]=='w':
                        break
                    if i+index<=9 and j+index<=9 and new_board[i+index][j+index]=='.':
                        new_board[i+index][j+index]='t'
                
                #print("queen move")
                #self.board_print(new_board)
                #break
            
            #enqueue
            for i in range(10):
                for j in range(10):
                    if new_board[i][j]=='t':
                        queue.append((i,j,number_interation))
            
            if not queue:
                break


        for i in range(10):
            for j in range(10):
                if new_board[i][j]=='.':
                    new_board[i][j]=100#100 stand for infinity, its mean the queen can't go to this place
        #print("queen move")
        #self.board_print(new_board)
        return new_board
        
    def king_move(self,state,site):
       
        new_board=self.board_copy(state)
        queue=deque()#very important structure
        number_interation=0
        
        for i in range(10):
            for j in range(10):
                if new_board[i][j]==site:
                    queue.append((i,j,0))
        
        while True:
            number_interation+=1
            while queue:
                (i,j,numberMove)=queue.popleft()

                if numberMove != 0:
                    new_board[i][j]=numberMove

                if i-1>=0 and new_board[i-1][j]=='.':
                    new_board[i-1][j]='t'
                if i+1<=9 and new_board[i+1][j]=='.':
                    new_board[i+1][j]='t'
                if j-1>=0 and new_board[i][j-1]=='.':
                    new_board[i][j-1]='t'
                if j+1<=9 and new_board[i][j+1]=='.':
                    new_board[i][j+1]='t'
                if i-1>=0 and j-1>=0 and new_board[i-1][j-1]=='.':
                    new_board[i-1][j-1]='t'
                if i-1>=0 and j+1<=9 and new_board[i-1][j+1]=='.':
                    new_board[i-1][j+1]='t'
                if i+1<=9 and j-1>=0 and new_board[i+1][j-1]=='.':
                    new_board[i+1][j-1]='t'
                if i+1<=9 and j+1<=9 and new_board[i+1][j+1]=='.':
                    new_board[i+1][j+1]='t'
            
            #enqueue
            for i in range(10):
                for j in range(10):
                    if new_board[i][j]=='t':
                        queue.append((i,j,number_interation))

            if not queue:
                break
        
        for i in range(10):
            for j in range(10):
                if new_board[i][j]=='.':
                    new_board[i][j]=100#100 stand for infinity, its mean the queen can't go to this place
        #print("king move")
        #self.board_print(new_board)
        return new_board

    def board_copy(self,board):
        new_board = [[]]*10
        for i in range(10):
            new_board[i] = [] + board[i]
        return new_board

    def board_print(self,board):
        for i in [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]:
            print(i, ":", end=" ")
            for j in range(10):
                print(board[i][j], end=" ")
            print()
        print("   ", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
        print("")

