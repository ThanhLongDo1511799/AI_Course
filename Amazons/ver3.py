
#===========================================#
#Copyright:                                 #
#Name:  ThanhLongDo                         #
#Private mail: long.dodg@gmail.com          #
#Student's ID: 1511799                      #
#===========================================#

from threading import Lock,Thread
import threading
import time
import math
from collections import deque

#========global variable===========
move_list=deque()# list of all node in 1 layer in tree
resultMove=[]#movement
lock=Lock()# for synchronazition
# ======================== Class Player =======================================
class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name

    def minMaxTree(self,state,AI_turn):
        global move_list,resultMove
        resultMove=[]
        move_list=deque()
       
        move_list_thread=generate_node(self.str,state)
        valuate_thread=valuation_thread(self.str,state)
        
        #start thread
        valuate_thread.start()
        move_list_thread.start()
        
        #waiting for thread
        move_list_thread.join(2.8)
        valuate_thread.join(2.8)
        
        
        if not resultMove:#AI_lose
            return []

        if valuate_thread.is_alive() or move_list_thread.is_alive():
            return [resultMove[0],resultMove[1],resultMove[2]]

        if (not valuate_thread.is_alive()) and (not move_list_thread.is_alive()):
            pass
            
        
        
        return [resultMove[0],resultMove[1],resultMove[2]]

    def nextMove(self, state):
        return self.minMaxTree(state,True)

#=========thread class for valuate a state===========================
class valuation_thread(threading.Thread):
    def __init__(self,str,state):
        threading.Thread.__init__(self)
        self.str=str
        self.state=state
        self.exit_flag=0

    def run(self):
        global resultMove
        resultMove=[]
        while True:
            if (not move_list) and self.exit_flag==3:
                break
            elif not move_list:
                self.exit_flag+=1
                #print("hello")
                time.sleep(0.001)
            else:
                self.exit_flag=0
                #use lock for synchronization purpose
                global lock
                lock.acquire()
                move=move_list.pop()
                lock.release()
                self.valuation_func(self.state,True,move)
            

    def valuation_func(self,state,AI_turn,move):
        global resultMove
        #change board
        new_board=self.board_copy(state)
        new_board[move[0]][move[1]]='.'
        new_board[move[2]][move[3]]=self.str
        
        #calculate individual value
        value1=self.t1_c1_w(new_board,AI_turn)

        #value2=self.t2_c2(new_board,AI_turn)
        t1=value1[0]
        c1=value1[1]
        w=value1[2]
        #t2=value2[0]
        #c2=value2[1]

        #heuristic function. This is the heart of program. be careful!
        value_func=t1#(1-w/70)*t1+(w/70)*c1+(w/70)*t2+(w/70)*c2
        
        if (not resultMove ) or value_func>resultMove[3]:
            resultMove=[[move[0],move[1]],[move[2],move[3]],[move[4],move[5]],value_func]
       
    #calculate t1,c1,w. Read article for more information
    def t1_c1_w(self,state,AI_turn):#for queen terrority
        value_t1=0
        value_c1=0
        w=0

        if self.str =='w':
            AI_state=self.queen_move(state,"w")
            opposite_state=self.queen_move(state,'b')
        else:
            AI_state=self.queen_move(state,"b")
            opposite_state=self.queen_move(state,'w')
        
        #self.board_print(state)
        #print("ai state:")
        #self.board_print(AI_state)
        #print("opposite state")
        #self.board_print(opposite_state)
        #print("value:",value_t1)
        for i in range(10):
            for j in range(10):
                if state[i][j]=='.':
                    #calculate w value
                    #w+=math.pow(2,-abs(AI_state[i][j] - opposite_state[i][j]))
                    #calculate c1 value
                    #value_c1+=2*(math.pow(2,-AI_state[i][j])- math.pow(2,-opposite_state[i][j]))
                    #caculate t1 value
                    if AI_state[i][j] == opposite_state[i][j] and AI_state[i][j]==1000:
                        value_t1+=0
                    elif AI_state[i][j] == opposite_state[i][j] and AI_turn:
                        value_t1+=0.2
                    elif AI_state[i][j] == opposite_state[i][j] and not AI_turn:
                        value_t1-=0.2
                    elif AI_state[i][j] < opposite_state[i][j]:
                        value_t1+=1
                    elif AI_state[i][j] > opposite_state[i][j]:
                        value_t1-=1
                    else:
                        value_t1+=0#do nothing
                    #print("AI_state[",i,"][",j,"]:",AI_state[i][j],"opposite_state[i][j]",opposite_state[i][j])
                    #print("value:",value_t1)
        
        return [value_t1,value_c1,w]   

    #calculate t2,c2. Read article for more information
    def t2_c2(self,state,AI_turn):#for king terrority
        value_t2=0
        value_c2=0

        if self.str =='w':
            AI_state=self.king_move(state,"w")
            opposite_state=self.king_move(state,'b')
        else:
            AI_state=self.king_move(state,"b")
            opposite_state=self.king_move(state,'w')
        #print("ai state:")
        #self.board_print(AI_state)
        #print("opposite state")
        #self.board_print(opposite_state)
        for i in range(10):
            for j in range(10):
                if state[i][j]=='.':
                    #calculate c2
                    value_c2+=self.min(1,self.max(-1, (opposite_state[i][j]-AI_state[i][j])/6 ))
                    #calculate t2
                    if AI_state[i][j] == opposite_state[i][j] and AI_state[i][j]==1000:
                        value_t2+=0
                    elif AI_state[i][j] == opposite_state[i][j] and AI_turn:
                        value_t2+=0.2
                    elif AI_state[i][j] == opposite_state[i][j] and not AI_turn:
                        value_t2-=0.2
                    elif AI_state[i][j] < opposite_state[i][j]:
                        value_t2+=1
                    elif AI_state[i][j] > opposite_state[i][j]:
                        value_t2-=1
                    else:
                        value_t2+=0#do nothing
                        
        #print("value t2:")
        #print(value_t2)
        return [value_t2,value_c2]

    def min(self,a,b):
        if a<b:
            return a
        return b
    
    def max(self,a,b):
        if a>b:
            return a
        return b

    def queen_move(self,state,site):
        new_board=self.board_copy(state)
        queue=deque()#very important structure
        number_interation=0

        for i in range(10):
            for j in range(10):
                if new_board[i][j]==site:
                    queue.append((i,j,0))

        while True:
            number_interation+=1
            
            while queue:
                (i,j,numberMove)=queue.popleft()

                if numberMove != 0:
                    new_board[i][j]=numberMove
                
                for index in range(1,10):
                    if i-index<0:
                        break
                    if new_board[i-index][j]=='X' or new_board[i-index][j]=='b' or new_board[i-index][j]=='w':
                        break
                    if i-index>=0 and new_board[i-index][j]=='.':
                        new_board[i-index][j]='t'
                    
                for index in range(1,10):
                    if i+index>9:
                        break
                    if new_board[i+index][j]=='X' or new_board[i+index][j]=='b' or new_board[i+index][j]=='w':
                        break
                    if i+index<=9 and new_board[i+index][j]=='.':
                        new_board[i+index][j]='t'

                for index in range(1,10):    
                    if j-index<0:
                        break
                    if new_board[i][j-index]=='X' or new_board[i][j-index]=='b' or new_board[i][j-index]=='w':
                        break
                    if j-index>=0 and new_board[i][j-index]=='.':
                        new_board[i][j-index]='t'
                
                for index in range(1,10):
                    if j+index>9:
                        break
                    if new_board[i][j+index]=='X' or new_board[i][j+index]=='b' or new_board[i][j+index]=='w':
                        break
                    if j+index<=9 and new_board[i][j+index]=='.':
                        new_board[i][j+index]='t'
                
                for index in range(1,10):
                    if i-index<0 or j-index<0:
                        break
                    if new_board[i-index][j-index]=='X' or new_board[i-index][j-index]=='b' or new_board[i-index][j-index]=='w':
                        break
                    if i-index>=0 and j-index>=0 and new_board[i-index][j-index]=='.':
                        new_board[i-index][j-index]='t'
              
                for index in range(1,10):
                    if i-index<0 or j+index>9:
                        break
                    if new_board[i-index][j+index]=='X' or new_board[i-index][j+index]=='b' or new_board[i-index][j+index]=='w':
                        break
                    if i-index>=0 and j+index<=9 and new_board[i-index][j+index]=='.':
                        new_board[i-index][j+index]='t'
                
                for index in range(1,10):
                    #print("i+index",i+index,"j-index",j-index)
                    if i+index>9 or j-index<0:
                        break
                    if new_board[i+index][j-index]=='X' or new_board[i+index][j-index]=='b' or new_board[i+index][j-index]=='w':
                        break
                    if i+index<=9 and j-index>=0 and new_board[i+index][j-index]=='.':
                        new_board[i+index][j-index]='t'
                
                for index in range(1,10):
                    if i+index>9 or j+index>9:
                        break
                    if new_board[i+index][j+index]=='X' or new_board[i+index][j+index]=='b' or new_board[i+index][j+index]=='w':
                        break
                    if i+index<=9 and j+index<=9 and new_board[i+index][j+index]=='.':
                        new_board[i+index][j+index]='t'
                
                #print("queen move")
                #self.board_print(new_board)
                #break
            
            #enqueue
            for i in range(10):
                for j in range(10):
                    if new_board[i][j]=='t':
                        queue.append((i,j,number_interation))
            
            if not queue:
                break


        for i in range(10):
            for j in range(10):
                if new_board[i][j]=='.':
                    new_board[i][j]=1000#1000 stand for infinity, its mean the queen can't go to this place
        #print("queen move")
        #self.board_print(new_board)
        return new_board
        
    def king_move(self,state,site):
       
        new_board=self.board_copy(state)
        queue=deque()#very important structure
        number_interation=0
        
        for i in range(10):
            for j in range(10):
                if new_board[i][j]==site:
                    queue.append((i,j,0))
        
        while True:
            number_interation+=1
            while queue:
                (i,j,numberMove)=queue.popleft()

                if numberMove != 0:
                    new_board[i][j]=numberMove

                if i-1>=0 and new_board[i-1][j]=='.':
                    new_board[i-1][j]='t'
                if i+1<=9 and new_board[i+1][j]=='.':
                    new_board[i+1][j]='t'
                if j-1>=0 and new_board[i][j-1]=='.':
                    new_board[i][j-1]='t'
                if j+1<=9 and new_board[i][j+1]=='.':
                    new_board[i][j+1]='t'
                if i-1>=0 and j-1>=0 and new_board[i-1][j-1]=='.':
                    new_board[i-1][j-1]='t'
                if i-1>=0 and j+1<=9 and new_board[i-1][j+1]=='.':
                    new_board[i-1][j+1]='t'
                if i+1<=9 and j-1>=0 and new_board[i+1][j-1]=='.':
                    new_board[i+1][j-1]='t'
                if i+1<=9 and j+1<=9 and new_board[i+1][j+1]=='.':
                    new_board[i+1][j+1]='t'
            
            #enqueue
            for i in range(10):
                for j in range(10):
                    if new_board[i][j]=='t':
                        queue.append((i,j,number_interation))

            if not queue:
                break
        
        for i in range(10):
            for j in range(10):
                if new_board[i][j]=='.':
                    new_board[i][j]=1000#1000 stand for infinity, its mean the queen can't go to this place
        #print("king move")
        #self.board_print(new_board)
        return new_board

    def board_copy(self,board):
        new_board = [[]]*10
        for i in range(10):
            new_board[i] = [] + board[i]
        return new_board

    def board_print(self,board):
        for i in [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]:
            print(i, ":", end=" ")
            for j in range(10):
                print(board[i][j], end=" ")
            print()
        print("   ", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
        print("")

#========thread class for generate node in tree triversal=============
class generate_node(threading.Thread):
    def __init__(self,str_name,state):
        threading.Thread.__init__(self)
        self.str=str_name
        self.state=state

    def run(self):
        #start=time.time()
        self.generate_move(self.state)
        #elapse = time.time() - start
        #print("eclapse time",elapse)
    #generate all possible move in one player    
    def generate_move(self,state):

        queen_list=deque()#list for saving queen position

        move=[[0,0],[0,0]]
        for i in range(10):
            for j in range(10):
                if state[i][j]==self.str:
                    queen_list.append((i,j))

        #print(queen_list)
        while queen_list:
            (i,j)=queen_list.popleft()
            move[0][0]=i;move[0][1]=j
            
            for index in range(1,10):
                if i-index<0:
                    break
                if state[i-index][j]=='X' or state[i-index][j]=='b' or state[i-index][j]=='w':
                    break
                if i-index>=0 and state[i-index][j]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j]=self.str
                    move[1][0]=i-index;move[1][1]=j
                    self.shoot(move,new_board)
                    #print("i,j:",i,j)
                    #print("i-index,j",i-index,j)
                    
            for index in range(1,10):
                if i+index>9:
                    break
                if state[i+index][j]=='X' or state[i+index][j]=='b' or state[i+index][j]=='w':
                    break
                if i+index<=9 and state[i+index][j]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j]=self.str
                    move[1][0]=i+index;move[1][1]=j
                    self.shoot(move,new_board)
                    
            for index in range(1,10):    
                if j-index<0:
                    break
                if state[i][j-index]=='X' or state[i][j-index]=='b' or state[i][j-index]=='w':
                    break
                if j-index>=0 and state[i][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i][j-index]=self.str
                    move[1][0]=i;move[1][1]=j-index
                    self.shoot(move,new_board)

            for index in range(1,10):
                if j+index>9:
                    break
                if state[i][j+index]=='X' or state[i][j+index]=='b' or state[i][j+index]=='w':
                    break
                if j+index<=9 and state[i][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i][j+index]=self.str
                    move[1][0]=i;move[1][1]=j+index
                    self.shoot(move,new_board)
                
            for index in range(1,10):
                if i-index<0 or j-index<0:
                    break
                if state[i-index][j-index]=='X' or state[i-index][j-index]=='b' or state[i-index][j-index]=='w':
                    break
                if i-index>=0 and j-index>=0 and state[i-index][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j-index]=self.str
                    move[1][0]=i-index;move[1][1]=j-index
                    self.shoot(move,new_board)

            for index in range(1,10):
                if i-index<0 or j+index>9:
                    break
                if state[i-index][j+index]=='X' or state[i-index][j+index]=='b' or state[i-index][j+index]=='w':
                    break
                if i-index>=0 and j+index<=9 and state[i-index][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i-index][j+index]=self.str
                    move[1][0]=i-index;move[1][1]=j+index
                    self.shoot(move,new_board)

            for index in range(1,10):
                if i+index>9 or j-index<0:
                    break
                if state[i+index][j-index]=='X' or state[i+index][j-index]=='b' or state[i+index][j-index]=='w':
                    break
                if i+index<=9 and j-index>=0 and state[i+index][j-index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j-index]=self.str
                    move[1][0]=i+index;move[1][1]=j-index
                    self.shoot(move,new_board)

            for index in range(1,10):
                if i+index>9 or j+index>9:
                    break
                if state[i+index][j+index]=='X' or state[i+index][j+index]=='b' or state[i+index][j+index]=='w':
                    break
                if i+index<=9 and j+index<=9 and state[i+index][j+index]=='.':
                    new_board=self.board_copy(state)
                    new_board[i][j]='.'
                    new_board[i+index][j+index]=self.str
                    move[1][0]=i+index;move[1][1]=j+index
                    self.shoot(move,new_board)

            #print(move_list)
            #file=open("result.txt","w")
            #file.write(str(move_list))
            #file.close()
            
        #print("move list",move_list)

    def shoot(self, move, state):
        global lock
        i=move[1][0];j=move[1][1]

        for index in range(1,10):
            if i-index<0:
                break
            if state[i-index][j]=='X' or state[i-index][j]=='b' or state[i-index][j]=='w':
                break
            if i-index>=0 and state[i-index][j]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j))
                lock.release()
                    
        for index in range(1,10):
            if i+index>9:
                break
            if state[i+index][j]=='X' or state[i+index][j]=='b' or state[i+index][j]=='w':
                break
            if i+index<=9 and state[i+index][j]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j))
                lock.release()
                    
        for index in range(1,10):    
            if j-index<0:
                break
            if state[i][j-index]=='X' or state[i][j-index]=='b' or state[i][j-index]=='w':
                break
            if j-index>=0 and state[i][j-index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i,j-index))
                lock.release()

        for index in range(1,10):
            if j+index>9:
                break
            if state[i][j+index]=='X' or state[i][j+index]=='b' or state[i][j+index]=='w':
                break
            if j+index<=9 and state[i][j+index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i,j+index))
                lock.release()
                
        for index in range(1,10):
            if i-index<0 or j-index<0:
                break
            if state[i-index][j-index]=='X' or state[i-index][j-index]=='b' or state[i-index][j-index]=='w':
                break
            if i-index>=0 and j-index>=0 and state[i-index][j-index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j-index))
                lock.release()
              
        for index in range(1,10):
            if i-index<0 or j+index>9:
                break
            if state[i-index][j+index]=='X' or state[i-index][j+index]=='b' or state[i-index][j+index]=='w':
                break
            if i-index>=0 and j+index<=9 and state[i-index][j+index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i-index,j+index))
                lock.release()

        for index in range(1,10):
            if i+index>9 or j-index<0:
                break
            if state[i+index][j-index]=='X' or state[i+index][j-index]=='b' or state[i+index][j-index]=='w':
                break
            if i+index<=9 and j-index>=0 and state[i+index][j-index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j-index))
                lock.release()
            
        for index in range(1,10):
            if i+index>9 or j+index>9:
                break
            if state[i+index][j+index]=='X' or state[i+index][j+index]=='b' or state[i+index][j+index]=='w':
                break
            if i+index<=9 and j+index<=9 and state[i+index][j+index]=='.':
                lock.acquire()
                move_list.append((move[0][0],move[0][1],move[1][0],move[1][1],i+index,j+index))
                lock.release()

    def board_copy(self,board):
        new_board = [[]]*10
        for i in range(10):
            new_board[i] = [] + board[i]
        return new_board

    def board_print(self,board):
        for i in [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]:
            print(i, ":", end=" ")
            for j in range(10):
                print(board[i][j], end=" ")
            print()
        print("   ", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
        print("")

