# ======================== Class Player =======================================
class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name

    def __str__(self):
        return self.str

    # Student MUST implement this function
    # The return value should be a move that is denoted by a list of tuples:
    # [(row1, col1), (row2, col2), (row3, col3)] with:
    # (row1, col1): current position of selected amazon
    # (row2, col2): new position of selected amazon
    # (row3, col3): position of the square is shot
    def nextMove(self, state):
        start = time.time()

        cpState = state_copy(state)
        if self.str == WHITE: othername = BLACK
        else: othername = WHITE
        current, other = getQueenLoca(self.str, othername, cpState)
        
        st = State()
        st.build(current, other, cpState, [] , start)

        result = search(st)
        if result == None:
            return None
        return result.move


# ======================== Game Object =======================================
import threading
from concurrent.futures import Future
import time
WHITE = 'w'
BLACK = 'b'
ARROW = 'X'
EMPTY = '.'
BOARDSIZE = 10
TIMEOUT = 2.7

def state_copy(board):
    new_board = [[]]*10
    for i in range(10):
        new_board[i] = [] + board[i]
    return new_board

def arrowChecker(row, col, lsarraow):
    for r, c in lsarraow:
        if r == row and c == col:
            return True
    return False

def available(queen, state, lsarrow):
    moves = []
    for i in range(1, BOARDSIZE):  # BOTTOMLEFT
        if queen[0] < i or queen[1] < i: break
        if arrowChecker(queen[0] - i, queen[1] - i, lsarrow): break
        if state[queen[0] - i][queen[1] - i] == EMPTY:
            moves.append([queen[0] - i, queen[1] - i])
        else:
            break
    for i in range(1, BOARDSIZE):  # BOTTOM
        if queen[0] < i: break
        if arrowChecker(queen[0] - i, queen[1], lsarrow): break
        if state[queen[0] - i][queen[1]] == EMPTY:
            moves.append([queen[0] - i, queen[1]])
        else:
            break
    for i in range(1, BOARDSIZE):  # BOTTOMRIGHT
        if queen[0] < i or queen[1] + i >= BOARDSIZE: break
        if arrowChecker(queen[0] - i, queen[1] + i, lsarrow): break
        if state[queen[0] - i][queen[1] + i] == EMPTY:
            moves.append([queen[0] - i, queen[1] + i])
        else:
            break
    for i in range(1, BOARDSIZE):  # RIGHT
        if queen[1] + i >= BOARDSIZE: break
        if arrowChecker(queen[0], queen[1] + i, lsarrow): break
        if state[queen[0]][queen[1] + i] == EMPTY:
            moves.append([queen[0], queen[1] + i])
        else:
            break
    for i in range(1, BOARDSIZE):  # TOPRIGHT
        if queen[0] + i >= BOARDSIZE or queen[1] + i >= BOARDSIZE: break
        if arrowChecker(queen[0] + i, queen[1] + i, lsarrow): break
        if state[queen[0] + i][queen[1] + i] == EMPTY:
            moves.append([queen[0] + i, queen[1] + i])
        else:
            break
    for i in range(1, BOARDSIZE):  # TOP
        if queen[0] + i >= BOARDSIZE: break
        if arrowChecker(queen[0] + i, queen[1], lsarrow): break
        if state[queen[0] + i][queen[1]] == EMPTY:
            moves.append([queen[0] + i, queen[1]])
        else:
            break
    for i in range(1, BOARDSIZE):  # TOPLEFT
        if queen[0] + i >= BOARDSIZE or queen[1] < i: break
        if not arrowChecker(queen[0] + i, queen[1] - i, lsarrow): break
        if state[queen[0] + i][queen[1] - i] == EMPTY:
            moves.append([queen[0] + i, queen[1] - i])
        else:
            break
    for i in range(1, BOARDSIZE):  # LEFT
        if queen[1] < i: break
        if arrowChecker(queen[0], queen[1] - i, lsarrow): break
        if state[queen[0]][queen[1] - i] == EMPTY:
            moves.append([queen[0], queen[1] - i])
        else:
            break
    return moves

def getQueenLoca(name, othername, state):
    current = []
    other = []
    for row in range(BOARDSIZE):
        for col in range(BOARDSIZE):
            if state[row][col] == name:
                current.append([row, col])
            elif state[row][col] == othername:
                other.append([row, col])
    return (current, other)

def call_future(func, future, args, kwargs):
    try:
        result = func(*args, **kwargs)
        future.set_result(result)
    except Exception as exc:
        future.set_exception(exc)

def threaded(func):
    def wrapper(*args, **kwargs):
        future = Future()
        thread = threading.Thread(target=call_future,
                         args =(func, future, args, kwargs))
        thread.start()
        return future
    return wrapper

class GameNode:
    def __init__(self, row, col, moveto = None):
        self.value = 0
        self.children = None
        self.move = [[row, col]]
        self.parent = None
        if moveto != None:
            self.move.append(moveto)
    def shootTo(self, row, col):
        self.move.append([row, col])

    def __lt__(self, other):
        return self.value < other.value

    def __eq__(self, other):
        if other == None: return False
        sm = self.move[0]
        om = other.move[0]
        return sm[0] == om[0] and sm[1] == om[1]

class State:
    def __init__(self):
        self.trees = []
        self.queens = []

    def build(self, current, other, board, used, starttime = 0.0):
        for r, c in current:
            boardcp = state_copy(board)
            node = GameNode(r, c)
            moves = available([r,c], boardcp, used)
            node.value = len(moves)
            self.queens.append(node)
            tree = GameTree()
            tree.buildTree(moves,[r,c],boardcp,other,used,starttime)
            self.trees.append(tree)

    def calValue(self, current):
        result = 0
        for queen in self.queens:
            result += queen.value
        if current != None:
            result += current.value
        for queen in self.queens:
            if queen == current:
                result = result - queen.value
        return result

class GameTree:
    def __init__(self):
        self.root = []

    def buildTree(self, moves, queen, board, otherQueens, used,  starttime):
        if otherQueens != 0:
            for r, c in moves:
                board[queen[0]][queen[1]] = EMPTY
                shoots = available([r, c], board, used)
                for srow, scol in shoots:
                    node = GameNode(queen[0], queen[1], [r, c])
                    node.shootTo(srow, scol)
                    currentused = used[:]
                    currentused.append([srow, scol])
                    node.value = len(available([r, c], board, currentused))
                    board[queen[0]][queen[1]] = EMPTY
                    child = self.buildSubTree(otherQueens,
                                              board,
                                              node,
                                              starttime)
                    node.children = child
                    self.root.append(node)
                    board[queen[0]][queen[1]] = ARROW
                    if time.time() - starttime >= TIMEOUT: return


    @threaded
    def buildTreeThreaded(self, moves,queen,board,otherQueens,used,starttime):
        self.buildTree(moves, queen, board, otherQueens, used, starttime)

    def buildSubTree(self, current, board, parent, starttime):
        state = State()
        state.build(current, 0, board, parent.move[1:], starttime)
        return state

def search(state):
    result = None
    best_value = -float('inf')
    successors = get_successors(state)
    for elem in successors:
        childState = elem.children
        value = state.calValue(elem) - childState.calValue(None)
        if value > best_value:
            best_value = value
            result = elem
    return result

def get_successors(state):
    successors = []
    for tree in state.trees:
        successors.extend(tree.root)
    return successors