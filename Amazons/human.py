

# ======================== Class Player =======================================
class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name

    def __str__(self):
        return self.str

    # Student MUST implement this function
    # The return value should be a move that is denoted by a list of tuples:
    # [(row1, col1), (row2, col2), (row3, col3)] with:
        # (row1, col1): current position of selected amazon
        # (row2, col2): new position of selected amazon
        # (row3, col3): position of the square is shot
    def nextMove(self, state):
        a=int(input("current position [0][0]"))
        b=int(input("current position [0][1]"))
        c=int(input("new position [1][0]"))
        d=int(input("new position [1][1]"))
        e=int(input("arrow position [2][0]"))
        f=int(input("arrow position [2][1]"))
        #result = [(0,3),(5,3),(8,6)] # example move in wikipedia
        return  [(a,b),(c,d),(e,f)]
