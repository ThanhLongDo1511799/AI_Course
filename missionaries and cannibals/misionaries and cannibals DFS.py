#ThanhLongDo-1511799 @email:1511799@hcmut.edu.vn
#missionaries and cannibals problem
#using DFS to go to solution in state space

import sys
class CandM:
    def __init__(self):  
        left_side=[0,0]#left_side[0] is missionary number,right_side[1] is cannibals number; in the left side of river
        right_side=[3,3]
        is_right=True#true if the boat in right side

        self.move(left_side,right_side,is_right)

    def move(self,left_side,right_side,is_right,id=0):#id variable to track the previous move style
        print("-----------------------------------")
        self.print(left_side,right_side,is_right)

        if self.goal_state(left_side,right_side,is_right)==True:#condition to exit
            print("succesfull!!!")
            sys.exit()
        elif self.check_valid(left_side,right_side)==False:
            print("illegal move")
            return None
        elif is_right==True:
            if id!=1:
                self.move(left_side=[x+1 for x in left_side],right_side=[x-1 for x in right_side],is_right=False,id=1)
            if id!=2:
                self.move(left_side=[left_side[0]+2,left_side[1]],right_side=[right_side[0]-2,right_side[1]],is_right=False,id=2)
            if id!=3:
                self.move(left_side=[left_side[0],left_side[1]+2],right_side=[right_side[0],right_side[1]-2],is_right=False,id=3)
            if id!= 4:
                self.move(left_side=[left_side[0],left_side[1]+1],right_side=[right_side[0],right_side[1]-1],is_right=False,id=4)
            if id!=5:
                self.move(left_side=[left_side[0]+1,left_side[1]],right_side=[right_side[0]-1,right_side[1]],is_right=False,id=5)
        else:
            if id!=1:
                self.move(left_side=[x-1 for x in left_side],right_side=[x+1 for x in right_side],is_right=True,id=1)
            if id!=2:
                self.move(left_side=[left_side[0]-2,left_side[1]],right_side=[right_side[0]+2,right_side[1]],is_right=True,id=2)
            if id!=3:
                self.move(left_side=[left_side[0],left_side[1]-2],right_side=[right_side[0],right_side[1]+2],is_right=True,id=3)
            if id != 4:
                self.move(left_side=[left_side[0],left_side[1]-1],right_side=[right_side[0],right_side[1]+1],is_right=True,id=4)
            if id!=5:
                self.move(left_side=[left_side[0]-1,left_side[1]],right_side=[right_side[0]+1,right_side[1]],is_right=True,id=5)
   
    def goal_state(self,left_side,right_side,is_right):
        if left_side[0]==left_side[1]==3 and right_side[0]==right_side[1]==0 and is_right==False:
            return True
        return False
    
    def check_valid(self,left_side,right_side):
        if(left_side[0]<0 or left_side[1]<0 or right_side[0]<0 or right_side[1]<0):
            return False
        elif (left_side[0]==0 and right_side[0]>=right_side[1]) or (right_side[0]==0 and left_side[0]>=left_side[1]):
            return True
        elif(left_side[0]<left_side[1] or right_side[0]<right_side[1]):
            return False
        return True
    
    def print(self,left_side,right_side,is_right):
        if is_right==False:
            print("(",left_side[0],"-",left_side[1],")<-----(",right_side[0],"-",right_side[1],")")
        else:
            print("(",left_side[0],"-",left_side[1],")----->(",right_side[0],"-",right_side[1],")")
        
CandM()


      
    
            

