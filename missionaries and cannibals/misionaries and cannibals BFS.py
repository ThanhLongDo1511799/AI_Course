#ThanhLongDo-1511799 @email:1511799@hcmut.edu.vn
#missionaries and cannibals problem
#using BFS to go to solution in state space

import sys
class CandM:
    def __init__(self):  
        list_state=self.initial_state()
        self.move(list_state)

    def initial_state(self):
        state=[0,0,3,3,True,0]
        #list[0] is the number of missionaries in the left side of the river
        #list[1]is the number of cannibals in the left side of the river
        #list[2] is the number of missionaries in the right side of the river
        #list[3] is the number of the cannibals in the right side of the river
        #list[4] is True if the boat in the right side of the river
        #list[5] is the backtracking of previous move
        list_state=[state]
        return list_state
    
    def move(self,list_state):
        while list_state:
            state=list_state.pop(0)
            is_right=state[4]
            id=state[5]
            if self.check_valid(state)==True:
                if self.goal_state(state)==True:
                    print(state)
                    print("successfull!!!")
                    sys.exit()
                elif is_right==True:
                    if id!=1:
                        list_state.append([state[0]+1,state[1]+1,state[2]-1,state[3]-1,False,1])
                    if id!=2:
                        list_state.append([state[0]+2,state[1],state[2]-2,state[3],False,2])
                    if id!=3:
                        list_state.append([state[0],state[1]+2,state[2],state[3]-2,False,3])
                    if id!=4 and id!=5:
                        list_state.append([state[0],state[1]+1,state[2],state[3]-1,False,4])
                        list_state.append([state[0]+1,state[1],state[2]-1,state[3],False,5])
                else:
                    if id!=1:
                        list_state.append([state[0]-1,state[1]-1,state[2]+1,state[3]+1,True,1])
                    if id!=2:
                        list_state.append([state[0]-2,state[1],state[2]+2,state[3],True,2])
                    if id!=3:
                        list_state.append([state[0],state[1]-2,state[2],state[3]+2,True,2])
                    if id!=4 and id!=5:
                        list_state.append([state[0]-1,state[1],state[2],state[3]+1,True,4])
                        list_state.append([state[0],state[1]-1,state[2],state[3]+1,True,5])

    def goal_state(self,state):
        if state[0]==state[1]==3 and state[2]==state[3]==0 and state[4]==False:
            return True
        return False
    
    def check_valid(self,state):
        if(state[0]<0 or state[1]<0 or state[2]<0 or state[3]<0):
            return False
        elif (state[0]==0 and state[2]>=state[3]) or (state[2]==0 and state[0]>=state[1]):
            return True
        elif(state[0]<state[1] or state[2]<state[3]):
            return False
        return True
    
    def print(self,state):
        if state[4]==False:
            print("(",state[0],"-",state[1],")<-----(",state[2],"-",state[3],")")
        else:
            print("(",state[0],"-",state[1],")----->(",state[2],"-",state[3],")")
        
CandM()


      
    
            

