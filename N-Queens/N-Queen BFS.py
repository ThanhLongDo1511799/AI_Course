#ThanhLongDo-1511799 @email:1511799@hcmut.edu.vn
#N-Queens problem
#using BFS to solve this problem

import sys
class NQueens:
    def __init__(self,size):  
        self.size=size

        list_state=self.initial_state(size)#initial state is list of list of list :))) (a bit complicated)
        self.move(list_state,size)

    def move(self,list_state,size):
        while list_state:
            temp=list_state.pop(0)
            index=temp[1]
            state=temp[0]

           # self.print_result(state)
            if self.goal_state(state)==True:# check the goal state
                self.print_result(state)
                print(state)
                print("Note: Nhin theo chieu ngang")
                sys.exit()       
            if index <size:
                for i in range(0,size):
                    #print("state[index], i",state[index],i)
                    if i>0:
                        state[index]=state[index]+1
                    state=state
                    list_state.extend([[state[:],index+1]])
           # print(list_state)
    
    def initial_state(self,size):
        state=[]
        for i in range(0,size):
            state.append(1)
        list=[[state,0]]
        return list

    def print_result(self,list):#print the chess board to the srceen
        for i in range(0,len(list)):
            for j in range(0,len(list)):
                if j==list[i]-1:
                    sys.stdout.write("  0")
                else:
                    sys.stdout.write("  x")
            sys.stdout.write("\n")
        print("-------------------------------")
    
    def goal_state(self,state):
        for index in range(1,len(state)):
            for i in range(0,index):#this for statement to check the quen in the same row
                if state[i]==state[index]:
                    return False
            
            for i in range(0,index):#this for statement to check the queen in the same diagonal line
                if state[i]==(state[index]+index-i) or state[i]==(state[index]-(index-i)):
                    return False
            
        return True
            
NQueens(7)
