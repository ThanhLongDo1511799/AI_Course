#ThanhLongDo-1511799 @email:1511799@hcmut.edu.vn
#N-Queens problem
#using DFS to solve this problem
#you can try with n=20 :)) AI will solve it in accepted period(about 20 seconds)
import sys
class NQueens:
    def __init__(self,size):  
        state=[]#define state
        self.size=size
                   
        self.initial_state(size,state)
        print(state)
        self.recursion_move(state,0)

    def initial_state(self,size,state):
        for index in range(0,size):
            state.append(1)

    def print_result(self,list):#print the chess board to the srceen
        for i in range(0,len(list)):
            for j in range(0,len(list)):
                if j==list[i]-1:
                    sys.stdout.write("  0")
                else:
                    sys.stdout.write("  x")
            sys.stdout.write("\n")
        print("-------------------------------")
            
    def legal_move_check(self,state,index):
        if index==0:
            return True
        
        for i in range(0,index):#this for statement to check the quen in the same row
            if state[i]==state[index]:
                return False
            
        for i in range(0,index):#this for statement to check the queen in the same diagonal line
            if state[i]==(state[index]+index-i) or state[i]==(state[index]-(index-i)):
                return False
            
        return True

    def recursion_move(self,list,index):
        state=list[:]
        #print(index)
        #self.print_result(state)
        
        for i in range(0,len(state)):
            if(self.legal_move_check(state,index)==True and index==len(state)-1):
                self.print_result(state)
                print(state)
                print("Note: nhin theo chieu ngang")
                sys.exit()
            elif self.legal_move_check(state,index)==True:
                self.recursion_move(state,index+1)
            state[index]=state[index]+1
           # print(index)
            #self.print_result(state)
            
NQueens(4)
